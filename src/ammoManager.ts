
export class AmmoManager{
    private _ammo_grap_quantity: number;
    private _ammo_coconut_quantity: number;

    private _ui_need_update: boolean;

    // constructor need shadow
    constructor() {
        this._ammo_grap_quantity = 0;
        this._ammo_coconut_quantity = 0;
        this._ui_need_update = true;
    }
    // pour mettre a jour l'interface
    public isUpdate(): boolean { 
        if (this._ui_need_update) {
            this._ui_need_update = false;
            return true;
        }
        return false;
    }
    
    // pour debug
    public give10() {
        this._ammo_grap_quantity = + 10;
        this._ammo_coconut_quantity = + 10;
        this._ui_need_update = true;
    }
    // throw seed
    public fire(index: number): boolean{
        if (index < 0 || index > 6) return false;
        let rv = true;
        switch (index) {
            case 0:
                if (this._ammo_grap_quantity > 0) {
                    this._ammo_grap_quantity -= 1;
                } else rv = false;
                break;
            case 1:
                if (this._ammo_coconut_quantity > 0) {
                    this._ammo_coconut_quantity -= 1;
                }
                else rv = false;
                break;
            default:
                break;
        }
        if(rv)
            this._ui_need_update = true;
        return rv;
    }
    public collect(index: number, quantity: number): boolean {
        if (quantity <= 0) return false;
        if (index < 0 || index > 1) return false;
        switch (index) {
            case 0:
                this._ammo_grap_quantity += quantity;
                break;
            case 1:
                this._ammo_coconut_quantity += quantity;
                break;
            default:
                break;
        }
        this._ui_need_update = true;
        return true;
    }
    // get ammo number by index
    public getAmmo(index: number): number {
        if (index < 0 || index > 1) return 0;
        switch (index) {
            case 0:
                return this._ammo_grap_quantity;
            case 1:
                return this._ammo_coconut_quantity;
        }
    }
}