export interface STAGE{
    IDWorld: number;
    IDLevel: number;
}

export class StageManager{
    private _needUpdate: boolean;
    private _qtyLvlInWorldOne: number;
    private _qtyLvlInWorldTwo: number;
    private _qtyWorld: number;
    private _currentSTAGE: STAGE;
    private _win: boolean;
    

    constructor() {
        this._needUpdate = false;
        this._qtyWorld = 2;
        this._qtyLvlInWorldOne = 5;
        this._qtyLvlInWorldTwo = 3;
        this._currentSTAGE = { IDWorld: 1, IDLevel: 1 };
        this._win = false;
    }
    /**
    * isUpdate
    * return true if the stage has changed
    * @param none
    * @return boolean
    */
    public isUpdate(): boolean {
        if (this._needUpdate) {
            this._needUpdate = false;
            return true;
        }
        return false;
    }
    /**
    * isWin
    * return true if the player has won
    * @param none
    * @return boolean
    */
    public isWin(): boolean {
        return this._win;
    }
    /**
    * nextLvl
    * load the next level
    * @param none
    * @return void
    */
    public nextLvl(): void{
        // last lvl -> trigger win
        if (this._currentSTAGE.IDWorld == this._qtyWorld && this._currentSTAGE.IDLevel == this._qtyLvlInWorldTwo) {
            this._win = true;
        } else
            if (this._currentSTAGE.IDWorld = 1) {
                if (this._currentSTAGE.IDLevel < this._qtyLvlInWorldOne)
                    this._currentSTAGE.IDLevel++;
                else {
                    this._currentSTAGE.IDWorld++;
                    this._currentSTAGE.IDLevel = 1;
                }
            } else
                if (this._currentSTAGE.IDWorld = 2) {
                    if (this._currentSTAGE.IDLevel < this._qtyLvlInWorldTwo)
                        this._currentSTAGE.IDLevel++;
                }
        this._needUpdate = true;
    }
    /**
    * goToLvl
    * load the level specified
    * @param world: number, lvl: number
    * @return boolean
    */
    public goToLvl(world: number, lvl: number): boolean {
        if (world < 1 || world > this._qtyWorld) return false;
        if (world == 1 && lvl > this._qtyLvlInWorldOne) return false;
        if (world == 2 && lvl > this._qtyLvlInWorldTwo) return false;
        this._currentSTAGE.IDWorld = world;
        this._currentSTAGE.IDLevel = lvl;
        this._needUpdate = true;
        return true;
    }
    /**
    * resetLvl
    * reset the current level
    * @param none
    * @return void
    */
    public resetLvl(): void {
        this._needUpdate = true;
    }
    /**
    * getSTAGE
    * return the current stage
    * @param none
    * @return STAGE
    */
    public getSTAGE(): STAGE {
        return this._currentSTAGE;
    }
}