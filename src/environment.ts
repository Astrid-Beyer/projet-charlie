import { Scene, Vector3, MeshBuilder, SceneLoader, StandardMaterial, Color3, Texture, FresnelParameters, TransformNode } from "@babylonjs/core";
import { StageManager } from "./stageManager";

export class Environment {
  private _scene: Scene;
  private _assets;
  private _seedAssets;
  private _stageManager: StageManager;

  constructor(scene: Scene, stagemanager: StageManager) {
    this._scene = scene;
    this._stageManager = stagemanager;
  }

  public setShadowGenerator(shadowGenerator) { 
    this._assets.allMeshes.forEach((m) => {
      shadowGenerator.addShadowCaster(m);
    });
    this._seedAssets.forEach((m) => {
      shadowGenerator.addShadowCaster(m);
    });
  }
  public async load() {
    //Create materials for the environment
    //this._envTest();

    const files = ["cubic_bushes", "jumping_plant", "waterlily"];
    this._seedAssets = [];
    for (let i = 0; i < files.length; i++) {
      let asset = await this._assetLoader("./models/model_textured_ready/seed/", files[i]);
      asset.allMeshes.forEach((m) => {
        m.position = new Vector3(0, 0, 0);
        m.isVisible = false;
        m.checkCollisions = false;
        m.receiveShadows = false;
        m.isPickable = false;
        if (m.name.includes("cubic_bushes")) {
          //m.material = stoneMaterial;
        } else
          if (m.name.includes("jumping_plant")) {
            //m.material = stoneMaterial;
          } else
            if (m.name.includes("waterlily")) {
              //m.material = stoneMaterial;
            }
        this._seedAssets.push(m);
      });
      
    }

    let tn = new TransformNode("seedobject", this._scene);
    


    this._assets = await this._loadAsset();
    let wallmaterial:StandardMaterial;
    let groundmaterial:StandardMaterial;
    let stoneMaterial: StandardMaterial;
    let mushhatmaterial: StandardMaterial;
    let mushrootmaterial: StandardMaterial;
    let pumpkinHatmaterial: StandardMaterial;
    let pumpkinRootmaterial: StandardMaterial;
    let waterMaterial: StandardMaterial;
    let treeleafMaterial: StandardMaterial;
    let treetrunkMaterial: StandardMaterial;
    
    wallmaterial = new StandardMaterial("material", this._scene);
    wallmaterial.diffuseTexture = new Texture("./models/textures/walltexture.png", this._scene);
    wallmaterial.specularTexture = new Texture("./models/textures/walltexture_heightmap.png", this._scene);
    

    pumpkinHatmaterial = new StandardMaterial("material", this._scene);
    pumpkinHatmaterial.diffuseTexture = new Texture("./models/textures/pumpkin_hat.png", this._scene);
    pumpkinHatmaterial.specularTexture = new Texture("./models/textures/pumpkin_hat_heightmap.png", this._scene);

    pumpkinRootmaterial = new StandardMaterial("material", this._scene);
    pumpkinRootmaterial.diffuseTexture = new Texture("./models/textures/pumpkin_root.png", this._scene);
    pumpkinRootmaterial.specularTexture = new Texture("./models/textures/pumpkin_root_heightmap.png", this._scene);

    treeleafMaterial = new StandardMaterial("material", this._scene);
    treeleafMaterial.diffuseTexture = new Texture("./models/textures/tree_leaf.png", this._scene);
    treeleafMaterial.specularTexture = new Texture("./models/textures/tree_leaf_heightmap.png", this._scene);

    treetrunkMaterial = new StandardMaterial("material", this._scene);
    treetrunkMaterial.diffuseTexture = new Texture("./models/textures/tree_trunk.png", this._scene);
    treetrunkMaterial.specularTexture = new Texture("./models/textures/tree_trunk_heightmap.png", this._scene);

    groundmaterial = new StandardMaterial("material", this._scene);
    groundmaterial.specularTexture = new Texture("./models/textures/grass_heightmap.png", this._scene);
    groundmaterial.diffuseTexture = new Texture("./models/textures/grass.png", this._scene);

    stoneMaterial = new StandardMaterial("material", this._scene);
    stoneMaterial.diffuseTexture = new Texture("./models/textures/stone.png", this._scene);
    stoneMaterial.specularColor = Color3.Black();
    mushhatmaterial = new StandardMaterial("material", this._scene);
    mushhatmaterial.diffuseTexture = new Texture("./models/textures/mush_hat.png", this._scene);
    mushhatmaterial.specularTexture = new Texture("./models/textures/mush_hat_heightmap.png", this._scene);
    mushrootmaterial = new StandardMaterial("material", this._scene);
    mushrootmaterial.diffuseTexture = new Texture("./models/textures/mush_root.png", this._scene);
    mushrootmaterial.specularTexture = new Texture("./models/textures/mush_root_heightmap.png", this._scene);
    
    waterMaterial = new StandardMaterial("material", this._scene);
    waterMaterial.diffuseTexture = new Texture("./models/textures/water.png", this._scene);
    waterMaterial.specularColor = Color3.Black();
    //Loop through all environment meshes that were imported
    this._assets.allMeshes.forEach((m) => {
      if (m.name.includes("ground")) {
        m.material = groundmaterial;
        m.checkCollisions = true;
      } else
        if (m.name.includes("scene")) {
          m.material = wallmaterial;
          m.checkCollisions = true;
          m.name = "wall";
        } else
          if (m.name.includes("mush")) {
            m.checkCollisions = true;
            if (m.name.includes("primitive0")) {
              m.material = mushhatmaterial;
            } else
              if (m.name.includes("primitive1")) {
                m.material = mushrootmaterial;
              }
        
          } else
            if (m.name.includes("stone1")) {
              m.material = stoneMaterial;
              m.checkCollisions = true;
            } else
              if (m.name.includes("stone2")) {
                m.material = stoneMaterial;
                m.checkCollisions = true;
              } else
                if (m.name.includes("stone0")) {
                  m.material = stoneMaterial;
                  m.checkCollisions = true;
                } else
                  if (m.name.includes("grass")) {
                    m.material = stoneMaterial;
                    m.checkCollisions = false;
                  } else
                    if (m.name.includes("pumpkin")) {
                      m.checkCollisions = true;
                      if (m.name.includes("primitive0")) {
                        m.material = pumpkinRootmaterial;
                      } else
                        if (m.name.includes("primitive1")) {
                          m.material = pumpkinHatmaterial;
                        }
                    } else
                      if (m.name.includes("Icosphere")) {
                        m.checkCollisions = true;
                        m.material = treeleafMaterial;
                      } else
                        if (m.name.includes("trunk")) {
                          m.checkCollisions = true;
                          m.material = treetrunkMaterial;
                        }else
                      if (m.name.includes("water")) {
                        m.material = waterMaterial;
                        m.checkCollisions = false;
                      }

      m.receiveShadows = true;
    });
  }

  //private load
  private _envTest() {
    //Create materials for the environment
    const boingMaterial = new StandardMaterial("boing", this._scene);
    boingMaterial.diffuseColor = Color3.Purple();
    const groundMaterial = new StandardMaterial("ground", this._scene);
    groundMaterial.diffuseColor = Color3.Green();
    const wallMaterial = new StandardMaterial("wall", this._scene);
    wallMaterial.diffuseColor = Color3.Red();
    const underWaterMaterial = new StandardMaterial("underwater", this._scene);
    underWaterMaterial.diffuseColor = Color3.Blue();
    //Create meshes for the environment
    var boing = MeshBuilder.CreateBox("boing", { size: 4 }, this._scene);
    boing.checkCollisions = true;
    boing.scaling = new Vector3(1, 1, 1);
    boing.position.y = 4;
    boing.position.x = -8;
    boing.material = boingMaterial;
    var ground = MeshBuilder.CreateBox("ground", { size: 4 }, this._scene);
    ground.checkCollisions = true;
    ground.scaling = new Vector3(1, 1, 1);
    ground.position.y = 4;
    ground.position.x = 0;
    ground.material = groundMaterial;
    var wall = MeshBuilder.CreateBox("wall", { size: 4 }, this._scene);
    wall.checkCollisions = true;
    wall.scaling = new Vector3(1, 1, 1);
    wall.position.y = 4;
    wall.position.x = 4;
    wall.material = wallMaterial;
    var underwater = MeshBuilder.CreateBox("underwater", { size: 4 }, this._scene);
    underwater.checkCollisions = true;
    underwater.scaling = new Vector3(1, 1, 1);
    underwater.position.y = 4;
    underwater.position.x = -4;
    underwater.material = underWaterMaterial;
  }

 

  //Load all necessary meshes for the environment
  private async _loadAsset() {
    const result = await SceneLoader.ImportMeshAsync(
      null,
      "./models/",
      "stage_" + this._stageManager.getSTAGE().IDWorld +
      "_" + this._stageManager.getSTAGE().IDLevel +
      "_filled" + ".glb",
      this._scene
    );

    let env = result.meshes[0];
    let allMeshes = env.getChildMeshes();

    return {
      env: env, //reference to our entire imported glb (meshes and transform nodes)
      allMeshes: allMeshes, // all of the meshes that are in the environment
    };
  }

  private async _assetLoader(path: string,namefile: string){
    const result = await SceneLoader.ImportMeshAsync(
      null,
      path,
      namefile + ".glb",
      this._scene
    );

    let env = result.meshes[0];
    let allMeshes = env.getChildMeshes();

    return {
      env: env, //reference to our entire imported glb (meshes and transform nodes)
      allMeshes: allMeshes, // all of the meshes that are in the environment
    };

  }

}
