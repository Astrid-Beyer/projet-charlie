import {
  TextBlock,
  StackPanel,
  AdvancedDynamicTexture,
  Image,
  Button,
  Rectangle,
  Control,
  DisplayGrid,
} from "@babylonjs/gui";
import { Scene } from "@babylonjs/core";
import { PlayerInput } from "./inputController";
import { AmmoSeedManager } from "./ammoSeedManager";
import { AmmoManager } from "./ammoManager";

export class Hud {
  private _scene: Scene;
  private _input: PlayerInput;
  private _playerUI: any; //UI Elements
  private _ammo_manager: AmmoManager;
  private _ammo_seed_manager: AmmoSeedManager;

  gamePaused: boolean;

  constructor(scene: Scene, input: PlayerInput) {
    this._scene = scene;
    this._input = input;
    this._createSelectSeedMenu();

    const playerUI = AdvancedDynamicTexture.CreateFullscreenUI("UI");
    this._playerUI = playerUI;
    this._playerUI.idealHeight = 720;
  }
  public setAmmoManager(ammo_manager: AmmoManager) {
    this._ammo_manager = ammo_manager;
  }
  public setAmmoSeedManager(ammo_seed_manager: AmmoSeedManager) { 
    this._ammo_seed_manager = ammo_seed_manager;
  }

  private _changeColors(ui: AdvancedDynamicTexture, selected: number, maxindex: number): void {
    for(let i = 0; i < maxindex; i++) {
      if (i == selected) {
        (ui.getControlByName("text" + i) as TextBlock).color  = "red";
      }else (ui.getControlByName("text" + i) as TextBlock).color  = "white";
    }
    //(ui.getControlByName("numberAmmo" + 2) as TextBlock).text = "4";
  }
  private _updateSeedAmmo(ui:AdvancedDynamicTexture, maxindex: number): void { 
    for(let i = 0; i < maxindex; i++) {
      (ui.getControlByName("numberAmmo" + i) as TextBlock).text = this._ammo_seed_manager.getAmmo(i).toString();
    }
  }
  private _createSelectSeedMenu() {
    const seedUI = AdvancedDynamicTexture.CreateFullscreenUI("UI",true,this._scene);
    seedUI.parseFromSnippetAsync("#XBTG4T#13").finally(() => {
      this._scene.registerBeforeRender(() => {
        this._changeColors(seedUI, this._input.chosen_seed, 5);
        if(this._ammo_seed_manager.isUpdate())
          this._updateSeedAmmo(seedUI,5);
      });
    });
  }
}
