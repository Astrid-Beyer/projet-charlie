import { Scene, MeshBuilder, PhysicsAggregate, PhysicsShapeType, Vector3, Quaternion, ActionManager, ExecuteCodeAction } from "@babylonjs/core";
import { Player } from "./characterController";
import { STAGE, StageManager } from './stageManager';
export class ActionEnvironement {
    private _scene: Scene;
    private _player: Player;
    private _stagemanager: StageManager;

    constructor(scene: Scene, player: Player, stagemanager: StageManager) {
        //init
        this._scene = scene;
        this._player = player;
        this._stagemanager = stagemanager;

        //add actions
        this._groundUseHavok();
        this._addRollingStone();
        this._addExitDoor();
        this._addDeathZone();
    }

    /**
    * addRollingStone
    * add a rolling stone to the scene, if the player enter collision with it, the level is reset
    * @param none
    * @return void
    */
    private _addRollingStone(): void{
        /*let c = MeshBuilder.CreateSphere("stone", { diameter: 4 }, this._scene);
        c.position = new Vector3(0, 0, 0);
        c.isVisible = false;
        c.checkCollisions = false;
        c.isPickable = false;

        let pushforce = 100;
        let spawn = this._scene.getTransformNodeByName("stoneSpawn");
        let stone = null;
        if (spawn != null) {
            let savedtime = Date.now();
            this._scene.registerBeforeRender(() => {
                let now = Date.now();
                if (savedtime + 1000 < now) 
                {
                    savedtime = now;
                    stone = null;
                    stone = c.clone("copy of "+c.name, null, false, false);
                    stone.position = spawn.getAbsolutePosition();
                    stone.isVisible = true;
                    stone.checkCollisions = true;
                    stone.isPickable = true;
                    stone.actionManager = new ActionManager(this._scene);
                    stone.actionManager.registerAction(
                        new ExecuteCodeAction(
                        {
                            trigger: ActionManager.OnIntersectionEnterTrigger,
                            parameter: this._player.getZone().parent,
                        },
                        () => {
                            console.log("reset level cause death");
                            this._stagemanager.resetLvl();
                            }
                        ));
                    const aggregate = new PhysicsAggregate(stone, PhysicsShapeType.SPHERE,  { mass: 1, center: new Vector3(0,0,0), rotation: Quaternion.Identity(), extents: new Vector3(.3, .3, .3) }, this._scene);
                    // add vertical impulse
                    let vecimp;
                    if (Math.abs(stone.absolutePosition.x) > Math.abs(stone.absolutePosition.z))
                        vecimp = new Vector3(stone.absolutePosition.x > 0 ? -pushforce : pushforce, 0, 0);
                    else
                        vecimp = new Vector3(0, 0, stone.absolutePosition.z > 0 ? -pushforce : pushforce);
                    
                    aggregate.body.applyImpulse(vecimp, stone.absolutePosition);
                    stone = null;
                }

            });
        }*/
    }

    /**
    * addExitDoor
    * add a door to the scene, if the player enter collision with it, the next level is loaded
    * @param none
    * @return void
    */
    private _addExitDoor() :void {
        let transform = this._scene.getTransformNodeByName("exit");
        if (transform != null) {
            let exitdoor = MeshBuilder.CreateBox("exit", { width: 1, depth: 5, height: 5 }, this._scene);
            exitdoor.position = transform.getAbsolutePosition();
            exitdoor.checkCollisions = false;
            exitdoor.isPickable = true;
            exitdoor.isVisible = true;
            exitdoor.actionManager = new ActionManager(this._scene);
            exitdoor.actionManager.registerAction(
                new ExecuteCodeAction(
                {
                    trigger: ActionManager.OnIntersectionEnterTrigger,
                    parameter: this._player.getZone(),
                },
                () => {
                    console.log("next level");
                    this._stagemanager.nextLvl();
                    }
            ));
        }
    }

    /**
    * addDeathZone
    * add a death zone to the scene, if the player enter collision with it, the level is reset
    * @param none
    * @return void
    */
    private _addDeathZone() : void { 
        let meshwater = this._scene.getMeshByName("water");
        if (meshwater != null) {
            meshwater.actionManager = new ActionManager(this._scene);
            meshwater.actionManager.registerAction(
                new ExecuteCodeAction(
                {
                    trigger: ActionManager.OnIntersectionEnterTrigger,
                    parameter: this._player.getZone().parent,
                },
                () => {
                    console.log("reset level cause death");
                    this._stagemanager.resetLvl();
                    }
            ));
        }
        let meshvoid = this._scene.getMeshByName("void");
        if (meshvoid != null) {
            meshvoid.actionManager = new ActionManager(this._scene);
            meshvoid.actionManager.registerAction(
                new ExecuteCodeAction(
                {
                    trigger: ActionManager.OnIntersectionEnterTrigger,
                    parameter: this._player.getZone().parent,
                },
                () => {
                    console.log("reset level cause death");
                    this._stagemanager.resetLvl();
                    }
            ));
        }

    }
    private _groundUseHavok(): void{
        const aggregateground = new PhysicsAggregate(
            this._scene.getMeshByName("ground"),
            PhysicsShapeType.MESH, { mass: 0 },
            this._scene);
        const aggregatewall = new PhysicsAggregate(
            this._scene.getMeshByName("wall"),
            PhysicsShapeType.MESH, { mass: 0 },
            this._scene);
    }


}