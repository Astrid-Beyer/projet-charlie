import {
    ActionManager,
    Color3,
    ExecuteCodeAction,
    Mesh,
  MeshBuilder,
  PhysicsAggregate,
  PhysicsShapeType,
  PickingInfo,
  Quaternion,
  Ray,
  RayHelper,
  Scene, StandardMaterial, Vector3,
} from "@babylonjs/core";
import { AmmoManager } from "../ammoManager";
export abstract class Seed{
    protected _scene: Scene;
    protected _id: number;
    protected _ammo: AmmoManager;
    protected readonly _mesh: Mesh;
    // constructor need shadow
    constructor(scene: Scene,ammo?:AmmoManager,zone?:Mesh) { 
        this._scene = scene;
        this._ammo = ammo;
        this._mesh = zone;
        this._id = -1;
    }
    
    public canHit(dir: Vector3, pos: Vector3): PickingInfo {
        //let ray = new Ray(pos, dir, 10000);
        let ray = this._scene.activeCamera.getForwardRay(undefined, undefined, pos);
        let hit = this._scene.pickWithRay(ray, this.predicate);
        return hit;
    }

    // throw seed
    public fire(hitinfo: PickingInfo): void{
        if(hitinfo.hit)
            this.bodyCollideCB(hitinfo);
    }
    // delete obj after delay for grapes & coconut
    protected deleteObjAfterDelay(obj: Mesh, delay: number):void { 
        setTimeout(function () {
            obj.dispose();
        }, delay);    
    }

    protected show(str:String) {
        console.log("print",str);
        console.log(this._scene);
        console.log(this._ammo);
        console.log(this._mesh);
        console.log(this._id);
    }
    protected generateObjectAfterDelay(obj: Mesh, delay: number, objtogenerate: Mesh, numberOfMesh: number): void {
        setTimeout(() => {
            for (let i = 0; i < numberOfMesh; i++){
                setTimeout(() => {
                    let c = objtogenerate.clone("copy of " + objtogenerate.name, objtogenerate.parent, false, false);
                    //let c = MeshBuilder.CreateBox("plutot ok"+i, {size: .5}, objtogenerate.getScene());
                    c.position = obj.position.add(new Vector3(0, 0, 0));//obj.position.add(new Vector3(numberOfMesh / 2 - i, 1, numberOfMesh / 2 - i));
                    c.checkCollisions = false;
                    c.isPickable = true;
                    c.isVisible = true;
                    c.rotate(new Vector3(0, 1, 0), (2 * Math.PI / numberOfMesh) * i);
                    if (this._ammo != null && this._mesh != null) {
                        // action manager
                        c.actionManager = new ActionManager(this._scene);
                        c.actionManager.registerAction(
                            new ExecuteCodeAction(
                                {
                                    trigger: ActionManager.OnIntersectionEnterTrigger,
                                    parameter: this._mesh
                                },
                                () => {
                                    if(this._ammo.collect(this._id, 1))
                                        c.dispose();
                                    else console.log("this._ammo.collect("+this._id+", 1) bug");
                                }
                            )
                        );
                    }


                    // add physics
                    const aggregate = new PhysicsAggregate(c, PhysicsShapeType.SPHERE,  { mass: 1, center: new Vector3(0,0,0), rotation: Quaternion.Identity(), extents: new Vector3(.3, .3, .3) }, this._scene);
                    // add vertical impulse
                    aggregate.body.applyImpulse(new Vector3(Math.cos((2*Math.PI / numberOfMesh) * i), 4, Math.sin((2*Math.PI / numberOfMesh) * i)), obj.absolutePosition);
                    // add rotation impulse
                    aggregate.body.setAngularVelocity(new Vector3(0, 10, 0));
                }
                    , i*300);
            }
            
        }, delay);    
    }

    // permet de filter les objets sur lesquels on peut tirer
    protected abstract predicate(mesh: Mesh): boolean;

    // callback quand on touche un objet
    protected abstract bodyCollideCB(collision: PickingInfo): void;

}