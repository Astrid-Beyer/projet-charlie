import { Seed } from "./seed";
import { Scene,Vector3,MeshBuilder, Ray, Mesh, StandardMaterial, Color3, RayHelper, PickingInfo } from '@babylonjs/core';

export class WallSeed extends Seed{
    constructor(scene: Scene) { 
        super(scene);
        this._id = 2;
    }
    override predicate(mesh: Mesh): boolean {
        if (mesh.name == "outer" || mesh.name == "ray" || mesh.name == "body_primitive0" || mesh.name == "wall" || mesh.name == "grass" || mesh.name == "water") {
            return false;
        } else return true;
    }
    override bodyCollideCB(collision: PickingInfo) {
        let platforme = this._scene.getTransformNodeByName("wallseed")?.getChildMeshes();
        //if (platforme != null) {
            let c = //platforme[0].clone("copy of " + platforme[0].name, this._scene.getTransformNodeByName("seedobject"),false) as Mesh;
            MeshBuilder.CreateBox("wall", { size: 4 }, this._scene);
            c.isVisible = true;
            c.checkCollisions = true;
            c.receiveShadows = true;
            c.isPickable = true;
            c.position = collision.pickedPoint;
            //Get the Samba animation Group
            //const growwall = this._scene.getAnimationGroupByName("grow_wallseed.001");
            //Play the Samba animation
            //growwall.start(false, 1.0, growwall.from, growwall.to, false);
            ///console.log(c.getAnimationByName("grow_wall"),"c.getAnimationByName(grow_wall)");
            //start(false, 1.0, growwall.from, growwall.to, false);
        //}

    }

}