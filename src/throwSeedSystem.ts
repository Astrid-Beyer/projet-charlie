import { Mesh, MeshBuilder, PickingInfo, Scene, Vector3 } from "@babylonjs/core";
import { Player } from "./characterController";
import { PlayerInput } from "./inputController";
import { CoconutTreeSeed } from "./seeds/coconutTreeSeed";
import { JumpSeed } from "./seeds/jumpSeed";
import { VineSeed } from "./seeds/vineSeed";
import { WallSeed } from "./seeds/wallSeed";
import { WaterLilySeed } from "./seeds/waterlilySeed";
import { AmmoSeedManager } from './ammoSeedManager';
import { AmmoManager } from "./ammoManager";

export class ThrowSeedSystem{
    private readonly _input: PlayerInput;
    private readonly _player: Player;
    private readonly _zone: Mesh;
    private _scene: Scene;
    private _ammo_seed_manager: AmmoSeedManager;
    private _ammo_manager: AmmoManager;

    // osef qu'il n'est pas unique a
    //chaque graine vue qu'on est plus dans un mode construction qu'autre chose
    // ça n'aurait pas beaucoup de sens
    private readonly _throwCD: number = 0.5;

    // permet de savoir si on peut tirer ou non en fonction du cooldown
    private _time_stored: number = 0;
    // constructor need shadow
    constructor(scene: Scene, player: Player, input: PlayerInput, ammo_manag: AmmoManager, ammo_seed_manag: AmmoSeedManager) { 
        this._input = input;
        this._player = player;
        this._zone = player.getZone();
        this._scene = scene;
        this._ammo_manager = ammo_manag;
        this._ammo_seed_manager = ammo_seed_manag;
        
        this._updateFireSystem();
    }
    
    // permet de planter la graine choisie a l'emplacement de la camera
    private _fireSeed(dir: Vector3, pos: Vector3): void {
        /*this._seed = this._seeds[this._selectedSeed];
        this._seed.fireSeed(this.mesh, this._camRoot);*/
        let amo = null;
        switch (this._input.chosen_seed) { 
            case 0:
                // planter une graine de vigne genere 10 munitions de raisin
                amo = new VineSeed(this._scene,this._ammo_manager,this._zone);
                break;
            case 1:
                // planter une graine de noix de coco genere 3 munitions de noix de coco
                amo = new CoconutTreeSeed(this._scene,this._ammo_manager,this._zone);
                break;
            case 2:
                amo = new WallSeed(this._scene);
                break;
            case 3:
                amo = new JumpSeed(this._scene);
                break;
            case 4:
                amo = new WaterLilySeed(this._scene);
                break;
            case 5:
                //amo = new CreeperSeed(this.scene);
                amo = new WallSeed(this._scene)
                break;
            case 6:
                //amo = new IvySeed(this.scene);
                amo = new WallSeed(this._scene)
                break;
            default:
                amo = new WallSeed(this._scene);
                break;
        }
        let hit: PickingInfo = amo.canHit(dir, pos);
        if (hit.hit && this._ammo_seed_manager.fire(this._input.chosen_seed)) {
            amo.fire(hit);
            console.log("fire: " + amo.constructor.name);
        }
    }
    // update fire system
    private _updateFireSystem(): void { 
        this._scene.registerBeforeRender(() => {
            let now = Date.now();
            if (this._input.fireKeyDown && now - this._throwCD*1000 > this._time_stored) {
                this._time_stored = now;
                this._fireSeed(this._player.getCamRoot().forward, this._player.getCamRoot().position);
            }
        });
    }

}